import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent implements OnInit {
  @ViewChild('miFormulario') miFormulario!: NgForm;
  initForm = {
    producto: 'Boli',
    precio: 10,
    existencia: 10
  }
  constructor() { }

  ngOnInit(): void {
  }

  guardar(){
    console.log(this.miFormulario);
    
      console.log('Posteo correcto');
      this.miFormulario.resetForm({
        producto: 'Trompo',
        precio: 0,
        existencia: 0
      });
    
  }

  nombreValido(): boolean{
    return this.miFormulario?.controls.producto?.invalid && this.miFormulario?.controls.producto?.touched;
  }
  precioValido(): boolean{
    this.miFormulario?.controls.precio?.setErrors(null)
    return this.miFormulario?.controls.precio?.value < 0 && this.miFormulario?.controls.producto?.touched
  }

}

import { Component } from '@angular/core';

interface Persona {
  nombre: string;
  favoritos: Favorito[];
}

interface Favorito {
  id:number;
  nombre: string;
}

@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styles: [
  ]
})
export class DinamicosComponent{
  nuevoFav: string = ''
  persona: Persona = {
    nombre: 'Triple H',
    favoritos: [
      {id:1, nombre:'Metal Gear Solid'},
      {id:2, nombre:'Super Mario Bros'},
    ]
  }

  guardar(){
    console.log('post');
  }

  validarNombre(): boolean{
    return true;
  }

  eliminar(indice: number){
    this.persona.favoritos.splice(indice, 1);
  }

  agregarFav(){
    const nuevofav: Favorito = {
      id: this.persona.favoritos.length + 1,
      nombre: this.nuevoFav
    };

    this.persona.favoritos.push({...nuevofav});
    this.nuevoFav = '';
  }

}

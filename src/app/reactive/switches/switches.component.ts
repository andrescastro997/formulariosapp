import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styles: [
  ]
})
export class SwitchesComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) { }
  miFormulario: FormGroup = this.formBuilder.group({
    genero: [ 'M', Validators.required ],
    notificaciones: [true, Validators.required],
    terminos: [false, Validators.requiredTrue]

  });
  persona = {
    genero: 'F',
    notificaciones: true
  };
  ngOnInit(): void {
    this.miFormulario.reset(
      {
        ...this.persona,
       terminos: false
      });
      this.miFormulario.get('terminos')?.valueChanges.subscribe(newValue =>{
        console.log(newValue);
      });

      this.miFormulario.valueChanges.subscribe(({terminos, ...rest}) =>{
        // console.log(form);
        // delete form.terminos;
        this.persona = rest
      });
  }

  guardar(){
    const  formValue = {...this.miFormulario.value};
    delete formValue.terminos

    this.persona = formValue;
    console.log(formValue);
  }

}

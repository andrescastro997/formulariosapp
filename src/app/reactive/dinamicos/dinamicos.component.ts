import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl, Form } from '@angular/forms';

@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styles: [
  ]
})
export class DinamicosComponent implements OnInit {
  miFormulario: FormGroup = this.formBuilder.group({
    nombre:[, [Validators.required, Validators.minLength(3)]],
    favoritos: this.formBuilder.array([
    ['Metal gear solid', Validators.required],
    ['El mario bros', Validators.required]
    ], Validators.required)
  })

  nuevoFavorito: FormControl = this.formBuilder.control('', Validators.required)

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    console.log(this.miFormulario.controls);
  }




  campoEsValido(campo: string){
    return this.miFormulario.controls[campo].errors && this.miFormulario.controls[campo].touched
  }

  guardar(){
    if (this.miFormulario.invalid){
      this.miFormulario.markAllAsTouched()
      return;
    }
    console.log(this.miFormulario.value);
    this.miFormulario.reset();
  }

  get favoritosArr(){
    return (this.miFormulario.get('favoritos') as FormArray);
  }

  agregarFavorito(){
    if(this.nuevoFavorito.invalid){return;}
    this.favoritosArr.push(new FormControl(this.nuevoFavorito.value, Validators.required));
    console.log(this.miFormulario.value);
    this.nuevoFavorito.reset();
  }

  borrar(index: number){
    this.favoritosArr.removeAt(index)
  }
  

}
